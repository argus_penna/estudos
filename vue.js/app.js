/**
 * Created by Argus on 18/08/2016.
 */

Vue.filter('labelContasRestantes', function (value) {
    if (value === false) {
        return "nenhuma conta cadastrada";
    }
    if (value > 0) {
        return "existem " + value + " contas a serem pagas";
    } else if (value === 0) {
        return "nenhuma conta a pagar";
    }

});

Vue.filter('LabelPago', function (value) {
    if (value == false) {
        return "Não Paga";
    } else {
        return "Paga";
    }
});

var billListComponent = Vue.extend({
    template: `
        <style type="text/css">   
            .pago {
                color: limegreen;
            }        
            .naoPago {
                color: red;
            }
        </style>
        <table border="1" cellpadding="10">
            <thead>
            <tr>
                <th>#</th>
                <th>Data Pagamento</th>
                <th>nome</th>
                <th>Valor</th>
                <th>Paga?</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(index,conta) in contas">
                <td>{{index +1}}</td>
                <td>{{conta.data_vencimento}}</td>
                <td>{{conta.nome}}</td>
                <td>{{conta.valor  | currency "R$ " 2}}</td>
                <td class="minhaClasse" :class="{'pago': conta.status, 'naoPago': !conta.status}">
                    {{conta.status | labelPago}}
                </td>
                <td>
                    <a href="#" @click.prevent="carregarConta(conta)">Editar</a>
                    |
                    <a href="#" @click.prevent="excluirConta(index)">Excluir</a>
                </td>
            </tr>
            </tbody>
        </table>
    `,
    
    data: function () {
        return {
            contas: [
                { data_vencimento: "20/08/2016", nome: "Conta de Luz", valor: "230,00", status: 1 },
                { data_vencimento: "20/01/2017", nome: "Conta de Água", valor: "20,00", status: 0 },
                { data_vencimento: "10/10/2016", nome: "Gasolina", valor: "250,00", status: 1 },
                { data_vencimento: "01/02/2017", nome: "Conta de Telefone", valor: "300,00", status: 0 },
                { data_vencimento: "20/10/2016", nome: "Extras", valor: "30,00", status: 1 },
                { data_vencimento: "14/08/2016", nome: "Extras", valor: "40,00", status: 1 },
                { data_vencimento: "12/12/2016", nome: "Extras", valor: "100,00", status: 0 },
                { data_vencimento: "30/11/2016", nome: "Extras", valor: "120,00", status: 1 },
                { data_vencimento: "05/09/2016", nome: "Extras", valor: "1230,00", status: 0 }
            ]
        }
    },
    methods: {
        carregarConta: function (conta) {
            this.$parent.conta = conta;
            this.$parent.activedView = 1;
            this.$parent.formType = "update";
        },
        excluirConta: function (id) {
            var decisao = confirm("Deseja realmente excluir essa conta?");
            if (decisao == true) {
                this.contas.splice(id, 1);
            }
            this.$parent.activedView = 0;
        },
    },
});

var createBillComponent = Vue.extend({
    template: `
        <form name="form" @submit.prevent="submit">
            <label>Vencimento:</label>
            <input type="text" v-model="conta.data_vencimento">
            <br/><br/>
            <label>Nome:</label>
            <Select v-model="conta.nome">
                <option v-for="nome in nomes" value="{{nome}}">{{nome}}</option>
            </Select>
            <br/><br/>
            <label>Valor:</label>
            <input type="text" v-model="conta.valor">
            <br/><br/>
            <label>Pago?</label>
            <input type="checkbox" v-model="conta.status" v-bind:false-value="0" v-bind:true-value="1">
            <br/><br/>
            <input type="submit" value="Enviar">
    
        </form>
    `,
    props: ['conta', 'formType'],
    data: function () {
        return {
            nomes: [
                "Conta de Luz",
                "Conta de Água",
                "Conta de Telefone",
                "Gasolina",
                "Extras"
            ],
        }
    },
    methods: {
        submit: function () {
            if (this.formType == "insert") {
                this.$parent.$children[1].contas.push(this.conta);
            }
            this.conta = "Conta de Luz",
                "Conta de Água",
                "Conta de Telefone",
                "Gasolina",
                "Extras";
            this.$parent.activedView = 0;
        },
    },

});

var menuComponent = Vue.extend({
    data: function () {
        return {

        }
    },
    methods: {

    },
    template: `
    <nav>
        <ul>
            <li v-for="menu in menus">
                <a href @click.prevent="showView(menu.id)">{{menu.name}}</a>
            </li>
        </ul>
    </nav>
    `,
    data: function () {
        return {
            menus: [
                { id: 0, name: "Listar Contas" },
                { id: 1, name: "Criar Conta" }
            ],
        }
    },
    methods: {
        showView: function (id) {
            this.$parent.activedView = id;
            if (id == 1) {
                this.$parent.conta = {};
                this.$parent.formType = "insert";
            }
        },
    }
});

var appComponent = Vue.extend({
    components: {
        'menu-component': menuComponent,
        'bill-list-component': billListComponent,
        'create-bill-component': createBillComponent
    },
    template: `
    <h1>{{ title }}</h1>
    <h3 :class="['pago', {'zerada': status === false ,'naoPago': status > 0}]">{{status | labelContasRestantes}}</h3>
    <menu-component></menu-component>
    <div v-show="activedView == 0">
        <bill-list-component></bill-list-component>
    </div>
    <div v-show="activedView == 1">
        <create-bill-component v-bind:conta="conta" v-bind:form-Type="formType"></create-bill-component>
    </div>
`,
    data: function () {
        return {
            title: "contas a pagar",
            activedView: 0,
            formType: "insert",
            conta: {
                data_vencimento: "",
                nome: "",
                valor: "",
                status: false
            },
        };
    },
    computed: {
        status: function () {
            if (!this.contas.length) {
                return false;
            }
            var count = 0;
            for (var i in this.contas) {
                if (!this.contas[i].status) {
                    count++;
                }
            }
            return count;
        }
    },
    methods: {

    }
});
Vue.component('app-component', appComponent);
var app = new Vue({
    el: "#app",
});
